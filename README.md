# conkyrc-pinephone

A .conkyrc config file optimized for Pinephone use of conky (https://github.com/brndnmtthws/conky)

## Installation

### Install conky

On mobian, you can install conky with 

`sudo apt install conky`

You do not need the `conky-all` package for this config file but I think you will need `lm-sensors`.

### Install the .conkyrc file

Simply copy .conkyrc to `/home/mobian`.  If you are transferring via USB cable, you may end up transferring as root user, so then you would need to issue:

`chown mobian:mobian /home/mobian/.conkyrc`

You will be asked for your password.

## Usage

When conky is run under `mobian` user, it will read the `/home/mobian/.conkyrc` file automatically.  Nothing else needs to be done.

## Enhancement

To autostart, I just stuck these lines in ~/.profile

```
# Autostart conky
conky &
```

## Issues

* This does not look good with horizontal layout.  Note that the config could be changed for this situation, but I don't know if any way to make conky actually responsive to rotation.  Also I have not looked.

* I don't know the correct name of the wireless interface on the pinephone.  I'll update it when I know.  Or you could tell me.

